from turtle import *
from random import randint

z = randint(10, 40)

for i in range(0, z):
    x = randint(10, 100)
    forward(x)
    y = randint(10, 170)
    left(y)
