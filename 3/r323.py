from daten import punkte_liste
from turtle import *

def gehe_wenn_nah(x):
    if distance(x) <= 100:
        goto(x)
    else:
        goto(0, 0)
    
for n in punkte_liste:
    gehe_wenn_nah(n)
