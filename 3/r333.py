from daten import woerterbuch

def spiegeln(x):
    y = {i: j for j, i in x.items()}
    return y

print(spiegeln(woerterbuch))
