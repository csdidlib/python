from random import randint
x = randint(1, 100)
y = 0
while x != y:
    y = int(input())
    if x > y:
        if (x - 20) >= y:
            print("Viel zu klein")
        else:
            print("Zu klein")
    elif x < y:
        if (x + 20) <= y:
            print("Viel zu groß")
        else:
            print("Zu groß")
print("Herzlichen Glückwunsch")
