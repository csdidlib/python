from daten import woerterbuch
from random import randint
a = []
b = []
x = ""
for i in woerterbuch.keys():
    a.append(i)
for i in woerterbuch.values():
    b.append(i)
while x != "ende":
    i = randint(0, len(woerterbuch.keys()) - 1)
    x = input("Was heißt " + a[i] + "?").lower()
    if x == b[i]:
        print("Das ist richtig.")
    elif x != b[i] and x != "Ende.":
        print("Das ist falsch.")
print("Auf Wiedersehen.")
