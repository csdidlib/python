""" Snake-Spiel aus dem Kurs "Programmieren lernen mit Python" von openHPI
    https://open.hpi.de/courses/pythonjunior2020
"""

import turtle
import random
import time


# erstelle_turtle() verkürzt die Schreibarbeit für
# die Erstellung der verschiedenen Turtles im Spiel.
#
# Den letzten zwei Parametern von erstelle_turtle() wurden sogenannte
# Standardwerte hinzugefügt. Standardwerte (auch: default values) werden
# für die jeweiligen Parameter beim Aufruf der Funktion automatisch
# eingesetzt, wenn der Funktion nicht explizit Argumente für diese Parameter
# mitgegeben werden. Da die Steuerungsdreiecke die Mehrheit der
# Turtle-Elemente darstellen, wurden eine Dreiecksform und eine grüne
# Füllfarbe als Standardwerte für die Parameter shape und color gewählt.
def erstelle_turtle(x, y, rotationswinkel, shape="triangle", color="green"):
    element = turtle.Turtle()
    element.speed(0)  # Keine Animation, Turtle "springt" zum Zielpunkt
    element.shape(shape)
    element.color(color)
    element.right(rotationswinkel)  # Nur für grüne Steuerungsdreiecke relevant
    element.penup()
    element.goto(x, y)

    # Nur für Kopf relevant; "direction" ist nicht aus Turtle,
    # sondern eine Variable von uns, die wir "element" dynamisch zuweisen
    element.direction = "stop"

    return element


def nach_unten_ausrichten():
    if kopf.direction != "up":
        kopf.direction = "down"


def nach_rechts_ausrichten():
    if kopf.direction != "left":
        kopf.direction = "right"


def nach_links_ausrichten():
    if kopf.direction != "right":
        kopf.direction = "left"


def nach_oben_ausrichten():
    if kopf.direction != "down":
        kopf.direction = "up"


def interpretiere_eingabe(x, y):
    if x >= 150 and x <= 170 and y >= -190 and y <= -170:
        # Kürzer: 150 <= x <= 170 and -190 <= y <= -170
        nach_unten_ausrichten()
    elif x >= 170 and x <= 190 and y >= -170 and y <= -150:
        # Kürzer: 170 <= x <= 190 and -170 <= y <= -150
        nach_rechts_ausrichten()
    # TODO: elif-Bedingung für Ausrichtung nach links
    # TODO: elif-Bedingung für Ausrichtung nach oben


def kopf_bewegen():
    if kopf.direction == "down":
        y = kopf.ycor()
        kopf.sety(y - 20)

    if kopf.direction == "right":
        x = kopf.xcor()
        kopf.setx(x + 20)

    # TODO: if-Bedingung für nach links ("left") ausgerichteten Kopf

    # TODO: if-Bedingung für nach oben ("up") ausgerichteten Kopf


def koerper_bewegen():
    # Alternativ kann auch nur das letzte Element auf die Position des
    # Kopfes bewegt werden und die restlichen Segmente bleiben unverändert.
    for index in range(len(segmente) - 1, 0, -1):
        # TODO: Bewege segmente[index] an segmente[index - 1]

    # TODO: Überprüfe, ob Schlange nicht nur aus Kopf besteht
        # TODO: Wenn, dann bewege erstes Segment zum Kopf


def segmente_entfernen():
    # Verstecke und entferne Segmente
    for segment in segmente:
        segment.hideturtle()
        del segment
    segmente.clear()


def spiel_neustarten():
    # TODO: Kopf in der Mitte platzieren
    # TODO: Richtung auf "stop" setzen
    segmente_entfernen()
    # TODO: Ausgabe, dass Spielrunde vorbei ist


def checke_kollision_mit_fensterrand():
    if kopf.xcor() > 190 or kopf.xcor() < -190 or kopf.ycor() > 190 or kopf.ycor() < -190:
        spiel_neustarten()


def checke_kollision_mit_segmenten():
    for segment in segmente:
        if segment.distance(kopf) < 20:
            spiel_neustarten()


def checke_kollision_mit_essen():
    if kopf.distance(essen) < 20:

        # TODO: Teil 1 -> Bewege Essen initial an eine zufällige, neue
        #       Position (x, y) außerhalb der Steuerung
            # "außerhalb der Steuerung":
            #       x muss echt kleiner als 140 sein
            #       und y muss echt größer -140 sein
            # x und y nehmen Werte aus {-180, -160, -140, ..., 140, 160, 180} an

        # TODO: Teil 2 -> Lass die Schlange wachsen, indem ein neues,
        #       gelbes Segment erstellt und zur Liste "segmente" hinzugefügt wird


def wiederhole_spiellogik():
    # Damit das Spiel bis zu einer Niederlage läuft, wird der folgende
    # Code von wiederhole_spiellogik() in einer Endlosschleife aufgerufen
    while True:
        checke_kollision_mit_essen()
        checke_kollision_mit_fensterrand()

        koerper_bewegen()
        kopf_bewegen()
        checke_kollision_mit_segmenten()

        # Position der verschiedenen Turtle-Elemente aktualisieren
        turtle.update()

        # time.sleep() unterbricht die Ausführung des weiteren
        # Codes für die angegebene Anzahl an Sekunden
        # An dieser Stelle verlangsamt sleep() das Spiel, damit die Schlange
        # nicht aus dem Bildschirm laufen kann, bevor man sie sehen kann.
        time.sleep(0.15)


# Auf dem Spielfeld sichtbare Elemente definieren
rechts = erstelle_turtle(180, -160, 0)
unten = erstelle_turtle(160, -180, 90)
# TODO: Turtle namens "links" mit erstelle_turtle(x, y, rotationswinkel) erstellen
# TODO: Turtle namens "oben" mit erstelle_turtle(x, y, rotationswinkel) erstellen
essen = erstelle_turtle(0, 100, 0, "circle", "red")
kopf = erstelle_turtle(0, 0, 0, "square", "black")
segmente = []

# Spielbereich (das sich öffnende Fenster beim Ausführen dieser Datei) definieren
spielbereich = turtle.Screen()
spielbereich.title("Mein Snake-Spiel")
spielbereich.setup(width=430, height=430)

# Drücken der Pfeiltasten zur Richtungssteuerung registrieren
spielbereich.onkeypress(nach_oben_ausrichten, "Up")
spielbereich.onkeypress(nach_links_ausrichten, "Left")
spielbereich.onkeypress(nach_unten_ausrichten, "Down")
spielbereich.onkeypress(nach_rechts_ausrichten, "Right")
spielbereich.listen(0)

# Registrierung der Richtungssteuerung über das Anklicken der grünen Dreiecke
turtle.onscreenclick(interpretiere_eingabe)

# Turtle in der Mitte verbergen
turtle.hideturtle()

# Automatisches Aktualisieren der Turtle-Elemente ausschalten
turtle.tracer(False)

# Try-Except-Block fängt Beenden des Spiels ab
try:
    wiederhole_spiellogik()
except turtle.Terminator:
    print("Das Spiel wurde beendet.")
    # exit(0) beendet das Program sauber
    exit(0)
