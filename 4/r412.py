try:
    x = int(input("Erste Zahl?"))
    o = input("Rechenoperation?")
    y = int(input("Zweite Zahl?"))
    z = 0
    if o == '+':
        z = x + y
    elif o == '-':
        z = x - y
    elif o == '*':
        z = x * y
    elif o == '/':
        z = x / y
    print(z)
except ValueError:
    print("Fehler")
