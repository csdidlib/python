from funktionen import buchstabenanzahl_ist_gerade

satz = "Du Suchender hast Mut und wirst die linke Allee nicht einschlagen sondern den Schatz entdecken sonst haben wir ihn gefunden"
z = satz.split(' ')
for n in z:
    if n[1] == 'e' or buchstabenanzahl_ist_gerade(n):
        print(n)
